#!/usr/bin/env python
import rospy
import time
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_motor_cmd
from finch_bot.msg import finch_aux_cmd
from finch_bot.msg import finch_feedback
from finch_bot.msg import finch_color
from finch_bot.msg import finch_mfrc522
from finch_bot.msg import finch_follow_cmd



# Tune these
GAINS = [ 10., 10., 10.];
COLOR_THRESH = [0.36, 0.36, 0.36];
MAXSPEED = 0.45
DEADBAND = 0.0

#These are constants
MODE_STOP = finch_follow_cmd.MODE_STOP;
MODE_LINE = finch_follow_cmd.MODE_LINE;
MODE_DEAD = finch_follow_cmd.MODE_DEAD;

RED   = 0;
GREEN = 1;
BLUE  = 2;
CLEAR = 3;

COLOR_NAMES = ["RED", "GREEN", "BLUE"];

class finch_linefollow(object):

    def process(self):
        mc = finch_motor_cmd();
        ac = finch_aux_cmd();
        now = time.time();

        if(self.mode == MODE_STOP):
            mc.left = 0;
            mc.right = 0;

        elif(self.mode == MODE_LINE):
            mc.left  = min(MAXSPEED,
                           max(0,
                               DEADBAND + GAINS[self.color_mode] * max(0,
                                                     self.right_colors[self.color_mode] - COLOR_THRESH[self.color_mode])));
            mc.right = min(MAXSPEED,
                           max(0,
                               DEADBAND + GAINS[self.color_mode] * max(0,
                                                     self.left_colors[self.color_mode] - COLOR_THRESH[self.color_mode])));

        elif(self.mode == MODE_DEAD):
            now = time.time();
            if(now - self.turn_start > self.turn_time):
                self.mode = MODE_LINE;
                self.process();
                return
            mc.left  = self.left_mtr;
            mc.right = self.right_mtr;

        self.counter = self.counter + 1;
        if self.counter % 20 == 0 :
            rospy.loginfo("Left %3.2f Right %3.2f\n", mc.left, mc.right);
            if(self.mode == MODE_STOP):
                rospy.loginfo("Stopped\n");
            elif(self.mode == MODE_LINE):
                rospy.loginfo("Color %s\n", COLOR_NAMES[self.color_mode]);
            elif(self.mode == MODE_DEAD):
                rospy.loginfo("Reckon %f seconds\n", now - self.turn_start > self.turn_time);

        ac.led = [ 255 * mc.left, 0, 255*mc.left];
        self.pub.publish(mc)
        self.pub_aux.publish(ac)

    def sense_cb(self, fb):

        if (fb.tap and self.mode != MODE_STOP):
            rospy.loginfo("Tap");
            self.mode = MODE_STOP;

        self.process();

    def color_cb(self, msg):
        if(msg.id == 1):
            self.left_colors[RED]   = float(msg.rgbc[RED])   / float(msg.rgbc[CLEAR])
            self.left_colors[GREEN] = float(msg.rgbc[GREEN]) / float(msg.rgbc[CLEAR])
            self.left_colors[BLUE]  = float(msg.rgbc[BLUE])  / float(msg.rgbc[CLEAR])
        else:
            self.right_colors[RED]   = float(msg.rgbc[RED])   / float(msg.rgbc[CLEAR])
            self.right_colors[GREEN] = float(msg.rgbc[GREEN]) / float(msg.rgbc[CLEAR])
            self.right_colors[BLUE]  = float(msg.rgbc[BLUE])  / float(msg.rgbc[CLEAR])
            return

        self.process();

    def follow_cb(self, msg):
        self.mode = msg.mode;
        if(msg.mode == msg.MODE_STOP):
            pass
        elif(msg.mode == msg.MODE_LINE):
            self.color_mode = msg.color_mode;
        elif(msg.mode == msg.MODE_DEAD):
            self.turn_start = time.time();
            self.turn_time = msg.time;
            self.left_mtr = msg.left_mtr;
            self.right_mtr = msg.right_mtr;
        else:
            printf("Unknown msg mode? %s", msg.mode);

        self.process();


    def __init__(self, *args, **kwds):

        self.mode = MODE_STOP;
        self.color_mode = MODE_DEAD;

        self.turn_start = time.time();
        self.turn_time = 0;
        self.left_mtr = 0;
        self.right_mtr = 0;

        self.left_colors = [0, 0, 0];
        self.right_colors = [0, 0, 0];
        self.counter = 0

        rospy.init_node('finch_linefollow', anonymous=True)

        self.pub = rospy.Publisher('finch_motor_cmd', finch_motor_cmd, queue_size=10)
        self.pub_aux = rospy.Publisher('finch_aux_cmd', finch_aux_cmd, queue_size=10)

        rospy.Subscriber('finch_feedback', finch_feedback, self.sense_cb)
        rospy.Subscriber('finch_color', finch_color, self.color_cb)
        rospy.Subscriber('finch_follow', finch_follow_cmd, self.follow_cb)

        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fd = finch_linefollow();
