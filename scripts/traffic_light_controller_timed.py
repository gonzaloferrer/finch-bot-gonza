#!/usr/bin/env python
import rospy
import sys
import math
import yaml
from finch_bot.msg import traffic_light_cmd
from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker

def traffic_light_controller_timed():
    traffic_lights = rospy.get_param('traffic_lights')

    # pub = rospy.Publisher('traffic_light_cmd', traffic_light_cmd, queue_size=10)
    # rospy.init_node('traffic_light_cmd_spoofer', anonymous=True)
    #
    # tl_cmd = traffic_light_cmd()
    #
    # tl_cmd.id = int(sys.argv[1])
    # tl_cmd.rgb[0] = int(sys.argv[2])
    # tl_cmd.rgb[1] = int(sys.argv[3])
    # tl_cmd.rgb[2] = int(sys.argv[4])
    #
    # pub.publish(tl_cmd)

    #publish rviz markers
    topic = 'traffic_light_markers'
    publisher = rospy.Publisher(topic, MarkerArray)

    rospy.init_node('register')

    lightArray = MarkerArray()
    for t in traffic_lights:
        marker = Marker()
        marker.header.frame_id = "/world"
        marker.type = marker.SPHERE
        marker.action = marker.ADD
        marker.scale.x = 0.2
        marker.scale.y = 0.2
        marker.scale.z = 0.001
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 0.0

        marker.pose.position.x = t['x']
        marker.pose.position.y = t['y']
        marker.id = t['id'];
        lightArray.markers.append(marker)

    lightArray.markers[0].color.r = 0

    while not rospy.is_shutdown():
       # Publish the MarkerArray
       publisher.publish(lightArray)
       rospy.sleep(0.01)


if __name__ == '__main__':
    try:
        traffic_light_controller_timed()
    except rospy.ROSInterruptException:
        pass
