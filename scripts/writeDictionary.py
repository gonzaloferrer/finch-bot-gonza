#!/usr/bin/env python
import RPi.GPIO as GPIO
import MFRC522
import signal
import sys
continue_reading = True
dict_f = open(os.path.dirname(__file__) + '/rfid_dict', 'w')
# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()
    dict_f.write("}")

def MFRC522_Read(dict_f):
    MIFAREReader = MFRC522.MFRC522()
    dict_v={}
    cnt = 0
    while continue_reading:
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
        if status == MIFAREReader.MI_OK:
            print "Card detected"
        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()
        if status == MIFAREReader.MI_OK:
            id = str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])
            print "Card read UID: "+id
            if id in dict_v:
                print "-----------------------already see the card"
            else:
                dict_v[id] = cnt
                dict_f.write('"' + id + '"' + ":" + str(cnt) +  ",")
                dict_f.write("\n")
                cnt=cnt+1

if __name__ == '__main__':
    print "Press Ctrl-C to stop."
    dict_f.write("{")
    signal.signal(signal.SIGINT, end_read)
    MFRC522_Read(dict_f)
