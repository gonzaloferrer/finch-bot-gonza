#!/usr/bin/env python
import rospy
import time
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_mfrc522
from finch_bot.msg import finch_follow_cmd
from visualization_msgs.msg import Marker

# tune these
SPEED = 0.60;
TIME = 0.7;

#These are constants
MODE_STOP = finch_follow_cmd.MODE_STOP;
MODE_LINE = finch_follow_cmd.MODE_LINE;
MODE_DEAD = finch_follow_cmd.MODE_DEAD;

RED   = 0;
GREEN = 1;
BLUE  = 2;

BTN_RED   = 1;
BTN_GREEN = 0;
BTN_BLUE  = 2;

LEFT   = 0;
STRT = 1;
RIGHT  = 2;

COLOR_NAMES = ["RED", "GREEN", "BLUE"];

class finch_behavior(object):

    def localization_marker(self):
        m = Marker();
        m.header.frame_id = "/world";
        m.header.stamp = rospy.Time();
        m.id = 1; # TODO: use the robot ID here
        m.type = Marker.SPHERE;
        m.action = Marker.DELETE;
        m.pose.position.x = self.current_wpt.x;
        m.pose.position.y = self.current_wpt.y;
        m.pose.position.z = 1;
        m.pose.orientation.x = 0.0;
        m.pose.orientation.y = 0.0;
        m.pose.orientation.z = 0.0;
        m.pose.orientation.w = 1.0;

        m.scale.x = 1.0;
        m.scale.y = 1.0;
        m.scale.z = 0.1;

        m.color.a = 1.0;
        if(self.mode == MODE_STOP):
            m.color.r = 1.0;
            m.color.g = 0.0;
            m.color.b = 0.0;
        elif(self.mode == MODE_LINE):
            m.color.r = 0.0;
            m.color.g = 1.0;
            m.color.b = 0.0;
        elif(self.mode == MODE_DEAD):
            m.color.r = 0.5;
            m.color.g = 0.5;
            m.color.b = 0.5;

        return m;


    def publish(self):
        fc = finch_follow_cmd();
        fc.mode = self.mode;
        fc.color_mode = self.color_mode;
        if(self.mode == MODE_DEAD):
            self.mode = MODE_LINE;
            fc.time = TIME;
            fc.left_mtr = SPEED;
            fc.right_mtr = SPEED;
            if(self.turn_mode == LEFT):
                fc.left_mtr = 0;
            if(self.turn_mode == RIGHT):
                fc.right_mtr = 0;

        self.pub.publish(fc);

        # Draw localization solution (circle at the current node)
        # Circle is green for traveling, red for waiting, grey for stopped
        pose_marker = localization_marker();
        self.marker_pub.publish(pose_marker)

    def mfrc_cb(self, msg):

        # If we're at a new tag
        if(self.rfid != msg.uid and self.rfid != self.old_rfid):

            for wpt in self.waypoints:
                if wpt.id != self.rfid: #I'm localized
                    continue;

                # Record current waypoint for visualization purposes
                self.current_wpt = wpt;

                if wpt.traffic_light_id < 0: # Theres a light here
                    continue;

                while(True): # Wait for green
                    lights = rosparam.get_param('traffic_lights');
                    light = lights[wpt.traffic_light_id]
                    if(light.color == "green"):
                        break
                    self.mode = MODE_STOP;
                    self.publish();
                    rospy.sleep(0.1);


            self.mode = MODE_LINE;
            self.publish();


        self.old_rfid = self.rfid;
        self.rfid = msg.uid;

    def joy_cb(self, msg): #XXX not called

        if(msg.buttons[5]):
            self.mode = MODE_STOP;
        elif(msg.buttons[BTN_RED]):
            self.mode = MODE_LINE;
            self.color_mode = RED;
        elif(msg.buttons[BTN_GREEN]):
            self.mode = MODE_LINE;
            self.color_mode = GREEN;
        elif(msg.buttons[BTN_BLUE]):
            self.mode = MODE_LINE;
            self.color_mode = BLUE;
        elif(msg.axes[6] > 0.5):
            self.mode = MODE_DEAD;
            self.turn_mode = RIGHT;
        elif(msg.axes[6] < -0.5):
            self.mode = MODE_DEAD;
            self.turn_mode = LEFT;
        elif(msg.axes[7] > 0.5):
            self.mode = MODE_DEAD;
            self.turn_mode = STRT;
        else:
            return;



    def __init__(self, *args, **kwds):

        self.rfid = -1;
        self.old_rfid = -1;

        self.mode = MODE_STOP;
        self.color_mode = RED;
        self.turn_mode = LEFT;
        self.waypoints = rospy.get_param('waypoints');
        self.current_wpt = -1;


        rospy.init_node('finch_behavior', anonymous=True)

        self.pub = rospy.Publisher('finch_follow', finch_follow_cmd, queue_size=10)
        self.marker_pub = rospy.Publisher('finch_marker', Marker, queue_size=10)
        rospy.Subscriber('finch_mfrc', finch_mfrc522, self.mfrc_cb)

        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fd = finch_behavior();
