#!/usr/bin/python
import json
import rospy
import MFRC522
import signal
import os
from finch_bot.msg import finch_mfrc522

continue_reading  = True
def end_read(signal,frame):
    print "Ending Read"
    continue_reading = False

class finch_mfrc_reader(object):

    def mfrc522_read(self):
        rate = rospy.Rate(2) # 10hz
        while continue_reading and not rospy.is_shutdown():
            # Scan for cards
            (status,TagType) = self.mifareReader.MFRC522_Request(self.mifareReader.PICC_REQIDL)
            if status == self.mifareReader.MI_OK:
                #print "Card detected"
            # Get the UID of the card
                (status,uid) = self.mifareReader.MFRC522_Anticoll()
                if status == self.mifareReader.MI_OK:
                    #print "Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
                    strID = str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])
                    msg = finch_mfrc522()
                    msg.utime = rospy.Time.now()
                    if strID in self.Mifare:
                        msg.uid = self.Mifare[strID]
                        msg.long_id = strID
                        rospy.loginfo(msg)
                        self.pub.publish(msg)
                    else:
                        msg.uid = -1
                        msg.long_id = strID
                        print "-----------detect card, but id not in table------------"
                        rospy.loginfo(msg)
                        self.pub.publish(msg)
                else:
                    msg = finch_mfrc522()
                    msg.utime = rospy.Time.now()
                    msg.uid = -1
                    rospy.loginfo(msg)
                    self.pub.publish(msg)

            rate.sleep()

    def __init__(self):

        with open(os.path.dirname(__file__) + '/rfid_dict') as data_file:
            self.Mifare = json.load(data_file)
        print(self.Mifare);

        self.mifareReader = MFRC522.MFRC522();
        rospy.init_node('mfrc522', anonymous=True)
        self.pub = rospy.Publisher('finch_mfrc', finch_mfrc522, queue_size=10)
        self.mfrc522_read();

if __name__ == '__main__':
    try:
        signal.signal(signal.SIGINT,end_read)
        finch_m_reader = finch_mfrc_reader()
    except rospy.ROSInterruptException:
        pass
