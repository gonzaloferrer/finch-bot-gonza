#!/usr/bin/env python
import RPi.GPIO as GPIO
import MFRC522
import signal

continue_reading = True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()

def MFRC522_Read():
    MIFAREReader = MFRC522.MFRC522()
    while continue_reading:
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
        if status == MIFAREReader.MI_OK:
            print "Card detected"
        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()
        if status == MIFAREReader.MI_OK:
            print "Card read UID: "+str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])

if __name__ == '__main__':
    print "Press Ctrl-C to stop."
    signal.signal(signal.SIGINT, end_read)
    MFRC522_Read()

