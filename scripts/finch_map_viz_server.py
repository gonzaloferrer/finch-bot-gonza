#!/usr/bin/env python
import rospy
from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point

def marker_set_color(m, color):
    m.color.a = 1.0
    if color == "cyan":
        m.color.r = 0.0;
        m.color.g = 1.0;
        m.color.b = 1.0;
    elif color == "red":
        m.color.r = 1.0;
        m.color.g = 0.0;
        m.color.b = 0.0;
    elif color == "green":
        m.color.r = 0.0;
        m.color.g = 1.0;
        m.color.b = 0.0;
    elif color == "blue":
        m.color.r = 0.0;
        m.color.g = 0.0;
        m.color.b = 1.0;
    elif color == "yellow":
        m.color.r = 1.0;
        m.color.g = 1.0;
        m.color.b = 0.0;
    return m

def waypoint_to_marker(wpt):
    m = Marker()
    m.header.frame_id = "/world";
    m.header.stamp = rospy.Time();
    m.ns = "my_namespace";
    m.type = Marker.SPHERE;
    m.action = Marker.ADD;
    m.pose.position.x = wpt['x'];
    m.pose.position.y = wpt['y'];
    m.pose.position.z = 0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 2;
    m.scale.y = 2;
    m.scale.z = 1;

    # Waypoints themselves are cyan
    return marker_set_color(m, 'cyan')

def link_to_marker(wpt, path, waypoints):
    m = Marker()
    m.header.frame_id = "/world";
    m.header.stamp = rospy.Time();
    m.ns = "my_namespace";
    m.type = Marker.ARROW;
    m.action = Marker.ADD;

    pA = Point()
    pA.x = wpt['x']
    pA.y = wpt['y']
    pA.z = 0.0
    pB = Point()
    wpt_next = waypoints[path['next']-1];
    pB.x = wpt_next['x']
    pB.y = wpt_next['y']
    pB.z = 0.0

    m.points.append(pA);
    m.points.append(pB);

    # Model size of arrow
    m.scale.x = 1;
    m.scale.y = 2;
    m.scale.z = 0;

    color = "none"
    if 'east' in path:
        color = path['east']['color']
    elif 'west' in path:
        color = path['west']['color']
    elif 'north' in path:
        color = path['north']['color']
    elif 'south' in path:
        color = path['south']['color']

    return marker_set_color(m, color)


def map_viz():
    rospy.init_node('map_viz', anonymous=True)

    pub = rospy.Publisher('map', MarkerArray, queue_size=10)

    waypoints = rospy.get_param('waypoints');
    roads = rospy.get_param('roads');

    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():

        ma = MarkerArray();

        lights = rospy.get_param('traffic_lights');

        # draw circles for each marker with color from map
        for wpt in waypoints:
            wp_marker = waypoint_to_marker(wpt);
            wp_marker.id = len(ma.markers);
            ma.markers.append(wp_marker);

            # draw edges for each connection - separate for each link
            for path in wpt['paths']:
                link_marker = link_to_marker(wpt, path, waypoints)
                link_marker.id = len(ma.markers);
                ma.markers.append(link_marker);

        # draw traffic light states

        # Wait for next iteration
        pub.publish(ma)
        rate.sleep()

if __name__ == '__main__':
    try:
        print "Starting map visualization server"
        map_viz()
    except rospy.ROSInterruptException:
        pass